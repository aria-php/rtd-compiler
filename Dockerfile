FROM python:3.7-alpine

RUN apk add --update npm

RUN npm install -g jsdoc

RUN pip install -U sphinx

RUN pip install -U sphinx-rtd-theme

RUN pip install -U sphinx_js

RUN pip install -U sphinxcontrib-phpdomain